document.addEventListener('DOMContentLoaded', evt => {

    const tabsContainer = document.querySelector('.tabs')
    let tabList = document.querySelectorAll('.tabs-title')
    let tabItems = document.querySelectorAll('.tab-items')

    tabsContainer.addEventListener('click', event => {
        let target = event.target

        tabList.forEach((element) => {
            console.log(element)
            element.classList.remove('active');
        })

        target.classList.add('active');

        tabItems.forEach((el) => {
            if (el.dataset.name === target.dataset.name) {
                el.classList.add('act')
            } else {
                el.classList.remove('act')
            }
        })
    })

})